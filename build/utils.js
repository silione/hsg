const package = require('../package.json');
const banner = `${package.name} ${package.version} | built by ${package.author} | ${package.email}`;


exports.config = package;
exports.banner = banner;