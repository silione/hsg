const fs = require('fs');
const utils = require('./utils');
const rimraf = require('rimraf');
const path = require('path');
const mkdirp = require('mkdirp');
const glob = require('glob');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');
const sass = require('node-sass');
const onceImporter = require('node-sass-once-importer');
const cleanCss = require('clean-css');
const dateFormat = require('dateformat');
const inquirer = require('inquirer');


const stylesFolder = `${__dirname}/../src/assets/styles`;
const src = 'src/assets';
const dist = 'custom';
var globals = [];
var components = [];
var source = '';
var globalsList = buildList(`${stylesFolder}/globals`);
var componentsList = buildList(`${stylesFolder}/components`);


inquirer.prompt([
  {
    type: 'checkbox',
    message: 'Select css globals',
    name: 'globals',
    choices: globalsList.map(function(e){ return { 'name': e, 'checked': true } }),
    validate: function(answer) {
      if (answer.length < 1) {
        return 'You must choose at least one css globals';
      }

      return true;
    }
  },
  {
    type: 'checkbox',
    message: 'Select css components',
    name: 'components',
    choices: componentsList.map(function(e){ return { 'name': e } }),
    validate: function(answer) {
      if (answer.length < 1) {
        return 'You must choose at least one css component';
      }

      return true;
    }
  }
])
.then(function(answers){
  answers.globals.forEach(function(name){
    globals.push(name);
    source += `@import "${stylesFolder}/globals/${name}";\n`
  })

  answers.components.forEach(function(name){
    components.push(name);
    source += `@import "${stylesFolder}/components/${name}";\n`
  })

  build();
})
.catch(function(error){
  console.log('inqurier error');
  throw error;
})


function build() {
  // delete custom
  rimraf.sync(dist);

  // create folders
  mkdirp.sync(path.join(dist, 'fonts'));
  mkdirp.sync(path.join(dist, 'images'));
  mkdirp.sync(path.join(dist, 'styles'));

  // copy fonts
  glob.sync(`${src}/fonts/*.*`).forEach(file => {
    var d = path.join(dist, 'fonts', path.basename(file));
    fs.createReadStream(file).pipe(fs.createWriteStream(d));
  });

  // copy images
  glob.sync(`${src}/images/*.*`).forEach(file => {
    var d = path.join(dist, 'images', path.basename(file));
    fs.createReadStream(file).pipe(fs.createWriteStream(d));
  });



  // build styles
  sass.render({
    data: source,
    outFile: path.join(dist, 'styles', 'hsg.css'),
    sourceMap: true,
    sourceMapEmbed: true,
    outputStyle: 'expanded',
    importer: onceImporter()
  }, function(err, result) {
    if (err) {
      console.log('sass render error');
      throw err;
    }

    postcss([ autoprefixer ]).process(result.css, {from: undefined}).then(function (res) {
      if (res.warnings().length) {
        console.log('autoprefix error');
        throw res.warnings()[0].toString();
      }

      var css = `
/*!
  ${utils.banner}
  Custom build created on ${dateFormat(new Date(), 'dd/mm/yyyy HH:MM:ss')}

  Globals List: ${globals.join(', ')}
  Components List: ${components.join(', ')}
*/

${res.css}
`;

      fs.writeFile(path.join(dist, 'styles', 'hsg.css'), css, function(err) {
        if (err) {
          console.log('write css file error');
          throw err;
        }
      })

      fs.writeFile(path.join(dist, 'styles', 'hsg.map.css'), result.map, function(err) {
        if (err) {
          console.log('write css map file error');
          throw err;
        }
      })

      fs.writeFile(path.join(dist, 'styles', 'hsg.min.css'), new cleanCss({
        advanced: false,
        keepSpecialComments: 0,
        rebase: false,
        sourceMap: true
        }).minify(css).styles, (err) => {
          if (err) {
            console.log('write css min file error');
            throw err;
          }
        })
    });

  })
}



function buildList(folder) {
  var arr = [];
  fs.readdirSync(folder).forEach(file => {
    arr.push(file.replace('.scss', ''))
  })

  return arr;
}
