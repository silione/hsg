const fs = require('fs');
const archiver = require('archiver');
const utils = require('./utils');
const version = utils.config.version;
var output = fs.createWriteStream(`dist/hsg-${version}.zip`);
var archive = archiver('zip');

archive.pipe(output);
archive.directory('dist/assets/fonts/', 'fonts');
archive.directory('dist/assets/images/', 'images');
archiveStyle(['hsg', 'components', 'globals']);

archive.finalize();


function archiveStyle(filenames) {
  filenames.forEach(function(filename) {
    archive.file(`dist/assets/styles/${filename}.css`, {name: `/styles/${filename}.${version}.css`});
    archive.file(`dist/assets/styles/minify/${filename}.css`, {name: `/styles/${filename}.min.${version}.css`});
  })
}
