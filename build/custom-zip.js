const rimraf = require('rimraf');
const path = require('path');
const fs = require('fs');
const utils = require('./utils');
const archiver = require('archiver');
const version = utils.config.version;
var output = fs.createWriteStream(`custom/hsg.custom.${version}.zip`);
var archive = archiver('zip');
const dist = 'custom';


// zip
archive.pipe(output);
archive.directory(path.join(dist, 'fonts'), 'fonts');
archive.directory(path.join(dist, 'images'), 'images');
archive.file(path.join(dist, 'styles', 'hsg.css'), {name: `/styles/hsg.custom.${version}.css`});
archive.file(path.join(dist, 'styles', 'hsg.min.css'), {name: `/styles/hsg.custom.${version}.min.css`});
archive.file(path.join(dist, 'styles', 'hsg.map.css'), {name: `/styles/hsg.custom.${version}.map.css`});
archive.finalize();

// delete folders
rimraf.sync(path.join(dist, 'fonts'));
rimraf.sync(path.join(dist, 'styles'));
rimraf.sync(path.join(dist, 'images'));
