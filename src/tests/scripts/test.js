(function(){
  'use strict';

  var tests = [
    ' tests list ',
    'grid',
    'button',
    'inputgroup',
    'list',
    'form',
    'pagination',
    'utility',
    'text',
    'strip',
    'progress',
    'table',
    'column',
    'stepper',
    'top',
    'head',
    'check',
    'icon',
    'dl',
    'modal',
    'actions',
    'switch',
    'multi',
    'popover',
    'spinner',
    'panel',
    'tooltip'
  ];

  var pages = [
    {
      title: 'modals',
      items: [
        'creditcard',
        'uid',
        'enter',
        'details',
        'sms',
        'activate',
        'success',
        'rusure'
      ]
    },
    {
      title: 'layouts',
      items: [
        'blank'
      ]
    }
  ];


  var $body = document.querySelector('body');
  var $header = document.createElement('header');
  var pathnameArray = window.location.pathname.split('/');
  var testsField = '';
  var pagesField = '';


  // build tests select
  tests.sort().forEach(function(test){
    var selected = (pathnameArray[pathnameArray.length-1] == test.trim() + '.html') ? 'selected' : '';
    testsField += '<option ' + selected + ' value="' + test.trim() + '.html">' + test.trim() + '</option>';
  });
  testsField = '<div class="h-field h-field--nolabel"><select name="tests" id="tests" class="h-field__element">' + testsField + '</select><div class="h-field__line"></div></div>';


  // build pages select
  pages.sort().forEach(function(page){
    var title = page.title;
    page.items.forEach(function(item){
      var selected = (pathnameArray[pathnameArray.length-2] + '/' + pathnameArray[pathnameArray.length-1] == title + '/' + item.trim() + '.html') ? 'selected' : '';
      pagesField += '<option ' + selected + ' value="pages/' + title + '/' + item.trim() + '.html">' + title + ' / ' + item.trim() + '</option>';
    });
  });
  pagesField = '<div class="h-field h-field--nolabel"><select name="pages" id="pages" class="h-field__element"><option value="">Pages list</option>' + pagesField + '</select><div class="h-field__line"></div></div>';


  // append to page
  $header.classList.add('c-head');
  $header.innerHTML = '<div class="h-gr"><div class="h-cl-4">' + testsField + '</div><div class="h-cl-4">' + pagesField + '</div></div>';
  $header.addEventListener('change', function(e){
    if (e.srcElement.value) {
      window.location.href = '/tests/' + e.srcElement.value;
    }
  });
  $body.insertBefore($header, $body.firstChild);

})()
