
// Polyfills

// add object is empty check to object prototype
Object.prototype.isEmpty = function() {
    for(var key in this) {
        if(this.hasOwnProperty(key))
            return false;
    }
    return true;
}

// Object.assign polyfill
if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}

// add closest support
if (!Element.prototype.matches)
    Element.prototype.matches = Element.prototype.msMatchesSelector ||  Element.prototype.webkitMatchesSelector;

if (!Element.prototype.closest)
    Element.prototype.closest = function(s) {
        var el = this;
        if (!document.documentElement.contains(el)) return null;
        do {
            if (el.matches(s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
    };

// add is integer support
Number.isInteger = Number.isInteger || function(value) {
  return typeof value === 'number' &&
    isFinite(value) &&
    Math.floor(value) === value;
};



class Hmulti {
  /**
   * class defaults
   * @returns object
   */
  get defaults() {
    return {
      height: 240,
      placeholder: 'לחצ/י לבחירת אפשרויות',
      disableNative: false
    }
  }

  /**
   * Keyboard values
   * @returns object
   */
  get keys() {
    return {
      esc: 27,
      tab: 9,
      up: 38,
      down: 40,
      space: 32,
      enter: 13
    }
  }

  /**
   * Direction values
   * @returns object
   */
  get direction() {
    return {
      up: -1,
      down: 1
    }
  }

  /**
   * Style Class name values
   * @returns object
   */
  get classNames() {
    return {
      multi: 'h-multi',
      open: 'is-open',
      selected: 'is-selected',
      disabled: 'is-disabled',
      active: 'is-active',
      list: 'h-multi__list',
      listItem: 'h-multi__listitem',
      select: 'select[multiple]',
      options: 'h-multi__options',
      item: 'h-multi__item',
      items: 'h-multi__items',
      empty: 'h-multi__item--empty'
    }
  }

  get itemsData() {
    return this._data || [];
  }
  set itemsData(arr) {
    this._data = arr;
  }

  get element() {
    if (!this._element) {
      throw new Error('Component is missing the element itself to work with');
    }

    return this._element;
  }
  set element(value) {
    var e;

    if (typeof value == 'string') {
      e = document.querySelector(value);
    } else if (this._isElement(value)) {
      e = value;
    }

    if (!e) {
      throw new Error('Cant find matching element to the query you send to component on initialize');
    }

    this._element = e;
  }

  /**
   * options
   * @returns object
   */
  get options() {
    return this._options || this.defaults;
  }
  set options(value) {
    this._options = Object.assign({}, this.defaults, (typeof value === 'object') ? value : {});
  }


  constructor(element, config) {
    this.element = element;
    this.options = config;

    // this is needed so we can add and remove event attached on body
    this._closeOnClickOutsideFunction = this._closeOnClickOutside.bind(this);

    this.empty = `<span class=${this.classNames.empty}>${this.options.placeholder}</span>`;
    this.itemsData = this._buildDataObject();
    this._addNeededElements();

    this._buildOptionsList();
    this._buildItemsList();
    this._attachEvents();
  }

  /**
   * Check if object is DOM element. Helper function
   * @param {any} o - unknown object
   * @return {boolean}
   */
  _isElement(o){
    return (
        typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
        o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
    );
  }

  /**
   * Check device using useragent. Helper function
   * @param {string} device
   * @return {Boolean}
   */
  _isDevice(device) {
    switch (device) {
      case "ios":
       return /iPhone|iPad|iPod/i.test(navigator.userAgent);

      case "android":
       return /Android|android/i.test(navigator.userAgent);

      default:
       return /Android|android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
  }

  /**
   * Add missing elements inside html
   */
  _addNeededElements() {
    var select = this.element.querySelector(this.classNames.select);

    if (!select) {
      throw new Error('Missing select[multiple] element inside multi component');
    }

    if (!select.id.length) {
      throw new Error('Select[multiple] element must contain id');
    }

    select.setAttribute('tabindex', '-1');
    this._addElementsItems();
    this._addElementsOptions();
  }

  /**
   * Add items list element if needed
   * the selected items that appears as buttons
   */
  _addElementsItems() {
    var el;

    if (!this.element.querySelector(`.${this.classNames.items}`)) {
      el = document.createElement('div');
      el.className = this.classNames.items;
      el.setAttribute('tabindex', '0');
      this.element.appendChild(el);
    }
  }

  /**
   * Add options list element if needed
   * the select dropdown list
   */
  _addElementsOptions() {
    var el;

    if (!this.element.querySelector(`.${this.classNames.options}`)) {
      el = document.createElement('div');
      el.className = this.classNames.options;
      this.element.appendChild(el);
    }
  }

  /**
  * Create options object (data)
  * @returns {array}
  **/
  _buildDataObject() {
    var options = this.element.querySelectorAll('option');
    var i;
    var dataList = [];

    for (i = 0; i < options.length; ++i) {
      dataList.push(
        this._dataItem({
          name: options[i].innerText,
          value: options[i].value,
          selected: options[i].selected,
          disabled: options[i].disabled
        })
      )
    }

    return dataList;
  }

  /**
   * Build data item
   * @param {object} obj - object containing: name, value, selected, disabled
   * @returns {object}
   */
  _dataItem(obj) {
    return {
      name: (obj.name) ? obj.name : '',
      value: (obj.value) ? obj.value : '',
      selected: !!obj.selected,
      disabled: !!obj.disabled
    }
  }

  /**
  * Build options list (mimic select dropbox)
  **/
  _buildOptionsList() {
    var i;
    var id;
    var klass;
    var text = `<ul
                  class="${this.classNames.list}"
                  role="listbox"
                  aria-multiselectable="true"
                  style="max-height:${Number.isInteger(this.options.height) ? this.options.height+'px' : this.options.height}">`;

    for (i = 0; i < this.itemsData.length; ++i) {
      klass = [this.classNames.listItem];

      if (this.itemsData[i].selected) {
        klass.push(this.classNames.selected);
      }

      if (this.itemsData[i].disabled) {
        klass.push(this.classNames.disabled);
      }

      id = `${this.element.querySelector(this.classNames.select).id}-${i+1}`;
      text += `<li
                data-value="${this.itemsData[i].value}"
                class="${klass.join(' ')}"
                role="option"
                id="${id}"
                aria-selected="${this.itemsData[i].selected}"
                aria-disabled="${this.itemsData[i].disabled}">
                  ${this.itemsData[i].name}
                </li>`;
    }

    text += '</ul>';

    this.element.querySelector(`.${this.classNames.options}`).innerHTML = text;
  }

  /**
  * Build selected items list (appears as buttons)
  **/
  _buildItemsList() {
    var i, itemsHtml = [];

    for (i = 0; i < this.itemsData.length; ++i) {
      if (this.itemsData[i].selected) {
        itemsHtml.push(`<button
                          class="${this.classNames.item}"
                          data-value="${this.itemsData[i].value}">
                            ${this.itemsData[i].name}
                          </button>`);
      }
    }

    this.element.querySelector(`.${this.classNames.items}`).innerHTML = (itemsHtml.length) ? itemsHtml.join('') : this.empty;
  }

  /**
   * Attach events to multi component elements
   */
  _attachEvents() {
    this._attachEventClickOnItems();
    this._attachEventClickOnOptions();
    this._attachEventChangeSelect();
    this._attachEventKeydownOnItems();
  }

  /**
   * Attach click event on items
   */
  _attachEventClickOnItems() {
    var that = this;

    if (this._isDevice('android') && !this.options.disableNative) {
      // Android browser disabled the ability to focus on input via javascript
      // so we make the SELECT element above the multi html. any click on it will open the SELECT
      this.parts.select.style.zIndex = 1;

    } else {

      this.element.querySelector(`.${this.classNames.items}`).addEventListener('click', function(e) {
        // Click on button
        if (e.srcElement.nodeName == 'BUTTON' && e.srcElement.classList.contains(that.classNames.item)) {
          e.preventDefault();
          that._clickOnOption(e.srcElement, false);
        } else {
          // Click on items row in empty place
          if (that._isDevice('ios') && !that.options.disableNative) {
            this.element.querySelector(`${this.classNames.select}`).focus();
          } else {
            if (that._isOpen()) {
              that._closeOptionsList();
            } else {
              that._openOptionsList();
            }
          }
        }
      })
    }
  }

  /**
   * Attach change event on select
   */
  _attachEventChangeSelect() {
    var that = this;

    if (this._isDevice() && !this.options.disableNative) {
      this.parts.select.addEventListener('change', function(e) {
        var o, i;
        var multiSelectOptions = that.element.querySelectorAll('option');

        for (i = 0; i < multiSelectOptions.length; ++i) {
          o = this._dataItem({
            name: multiSelectOptions[i].innerText,
            value: multiSelectOptions[i].value,
            selected: multiSelectOptions[i].selected
          });

          that._setDataOptionSelected(o);
          that._buildItemsList();
        }
      });
    }
  }

  /**
   * Attach click event on options
   */
  _attachEventClickOnOptions() {
    var that = this;

    this.element.querySelector(`.${this.classNames.options}`).addEventListener('click', function(e) {
      if (e.srcElement.nodeName == 'LI') {
        e.preventDefault();
        that._clickOnOption(e.srcElement);
      }
    });
  }

  /**
   * Check if multi is open
   */
  _isOpen() {
    return this.element.classList.contains(this.classNames.open);
  }

  /**
   * Open options list
   **/
  _openOptionsList() {
    var item;

    this.element.classList.add(this.classNames.open);
    document.documentElement.addEventListener('keydown', this._closeOnKeydown.bind(this));
    document.body.addEventListener('click', this._closeOnClickOutsideFunction);

    // add active state on first available list item
    if (!this.element.querySelector(`.${this.classNames.active}`)) {
      item = this.element.querySelector(`.${this.classNames.listItem}:not(.${this.classNames.disabled})`);
      if (item) {
        item.classList.add(this.classNames.active);
      }
    }
  }

  /**
  * Close options list
  **/
  _closeOptionsList() {
    var active = this.element.querySelector(`.${this.classNames.active}`)

    document.documentElement.removeEventListener("keydown", this._closeOnKeydown.bind(this));
    document.body.removeEventListener("click", this._closeOnClickOutsideFunction);
    this.element.classList.remove(this.classNames.open);

    if (active) {
      active.classList.remove(this.classNames.active);
    }
  }

  /**
  * Close options list with ESC
  * @property {object} e - event
  **/
  _closeOnKeydown(e) {
    if ((e.keyCode === this.keys.esc || e.keyCode === this.keys.tab) && this.element.classList.contains(this.classNames.open)) {
      e.preventDefault();
      this._closeOptionsList();
    }
  }

  /**
  * Close options list (dropdown) when clicking outside options
  * @property {object} e - event
  **/
  _closeOnClickOutside(e) {
    var inside = ((e.srcElement.closest(`.${this.classNames.multi}`) === this.element) || (e.srcElement.classList && e.srcElement.classList.contains(this.classNames.listItem)));

    if (!inside) {
      this._closeOptionsList();
    }
  }

  /**
   * Add list option to items selected
   * @property {element} element - html element to get data from
   * @property {boolean} selected - whether the element will be added or removed
   */
  _clickOnOption(element, selected) {
    var obj;

    if (element && !element.classList.contains(this.classNames.disabled)) {
      obj = this._dataItem({
        name: element.innerText,
        value: element.getAttribute('data-value'),
        selected: (selected == null) ? !(element.classList.contains(this.classNames.selected)) : selected
      })

      this._setDataOptionSelected(obj);
      this._setSelectOptionSelected(obj);
      this._updateOptionsList();
      this._updateItemsList(obj);
    }
  }

  /**
  * Select/Unselect data option
  * @property {object} o - option
  **/
  _setDataOptionSelected(o) {
    var i;

    for (i = 0; i < this.itemsData.length; i++) {
      if (this.itemsData[i].name === o.name && this.itemsData[i].value === o.value) {
        this.itemsData[i].selected = o.selected;
        break;
      }
    }
  }

  /**
  * Select/Unselect select option
  * @property {object} o - option
  * @property {boolean} val - set as selected or not
  **/
  _setSelectOptionSelected(o) {
    var option = this.element.querySelector(`option[value="${o.value}"]`);

    if (option) {
      option.selected = o.selected;
    }
  }

  /**
   * Update options list
   */
  _updateOptionsList() {
    var i;
    var listItems = this.element.querySelectorAll(`.${this.classNames.listItem}`);
    var classList;

    for (i = 0; i < this.itemsData.length; ++i) {
      classList = listItems[i].classList;

      if (this.itemsData[i].selected) {
        classList.add(this.classNames.selected);
      } else {
        classList.remove(this.classNames.selected);
      }

      if (this.itemsData[i].disabled) {
        classList.add(this.classNames.disabled);
      } else {
        classList.remove(this.classNames.disabled);
      }

      listItems[i].setAttribute('aria-selected', this.itemsData[i].selected);
      listItems[i].setAttribute('aria-disabled', this.itemsData[i].disabled);
    }
  }

  /**
   * Update items list. add or remove item
   * @property {object} o - option
  **/
  _updateItemsList(o) {
    var element;
    var items = this.element.querySelector(`.${this.classNames.items}`);
    var item = this.element.querySelector(`.${this.classNames.item}[data-value="${o.value}"]`);

    if (o.selected) {
      if (!item) {
        // add button
        element = document.createElement('button');
        element.classList.add(this.classNames.item);
        element.setAttribute('data-value', o.value);
        element.innerText = o.name;

        // Clean items element when no buttons exists in it
        if (!this.element.querySelectorAll(`.${this.classNames.item}`).length) {
          while (items.firstChild) items.removeChild(items.firstChild);
        }

        items.appendChild(element);
      }
    } else {
      // Remove button if exists
      if (item) {
        item.parentNode.removeChild(item);

        // Add empty if no button exists
        if (!this.element.querySelectorAll(`.${this.classNames.item}`).length) {
          items.innerHTML = this.empty;
        }
      }
    }
  }

  /**
   * Attach keydown event on items
   * needed for accessibility support
   */
  _attachEventKeydownOnItems() {
    var that = this;
    var active;

    if (!this._isDevice() || this.options.disableNative) {
      this.element.querySelector(`.${this.classNames.items}`).addEventListener('keydown', function(e) {
        if (e.keyCode === that.keys.space) {
          e.preventDefault();
          if (that._isOpen()) {
            active = that.element.querySelector(`.${that.classNames.active}`);
            if (active) {
              that._clickOnOption(active);
            } else {
              that._closeOptionsList();
            }
          } else {
            that._openOptionsList();
          }
        }

        if (e.keyCode === that.keys.up) {
          e.preventDefault();
          if (that._isOpen()) {
            that._moveActive(that.direction.up);
          } else {
            that._openOptionsList();
          }
        }

        if (e.keyCode === that.keys.down) {
          e.preventDefault();
          if (that._isOpen()) {
            that._moveActive(that.direction.down);
          } else {
            that._openOptionsList();
          }
        }

        if (e.keyCode === that.keys.enter) {
          if (that._isOpen()) {
            active = that.element.querySelector(`.${that.classNames.active}`);
            if (active) {
              that._clickOnOption(active);
            } else {
              that._closeOptionsList();
            }
          }
        }
      });
    }
  }

  /**
   * Move active element up ro down
   * @property {value} direction - up or down
   */
  _moveActive(direction) {
    var active = this.element.querySelector(`.${this.classNames.active}`);
    var items = this.element.querySelectorAll(`.${this.classNames.listItem}`);
    var newIndex;

    newIndex = this._getNewIndex(items, active, direction);

    if (active) {
      active.classList.remove(this.classNames.active);
    }

    items[newIndex].classList.add(this.classNames.active);
  }

  /**
   * Calculate new index for active element
   * @property {array} items - items list
   * @property {element} item - html element inside list
   * @property {value} direction - up or down
   * @returns {number} new element index
   */
  _getNewIndex(items, item, direction) {
    var element;
    var list = Array.prototype.slice.call(items);
    var i = 0;

    if (direction === this.direction.up) { // up
      if (item) {
        if (list.indexOf(item)) { // not first element. go one up
          element = item.previousElementSibling;
          while(element && element.classList.contains(this.classNames.disabled)) {
            element = element.previousElementSibling;
          }
        } else { // first element. so we stay at first
          element = items[0];
        }
      } else {
        // get first element that is not disabled
        element = items[0];
        while(element && element.classList.contains(this.classNames.disabled)) {
          element = element.nextElementSibling;
        }
      }

    } else { // down
      if (item) { // not last element. go one down
        if (list.indexOf(item) !== (items.length - 1)) {
          element = item.nextElementSibling;
          while(element && element.classList.contains(this.classNames.disabled)) {
            element = element.nextElementSibling;
          }
        } else { // last element. so we stay at last
          element = items[items.length - 1];
        }
      } else {
        // get last element that is not disabled
        element = items[items.length-1];
        while(element && element.classList.contains(this.classNames.disabled)) {
          element = element.previousElementSibling;
        }
      }
    }

    return list.indexOf(element);
  }


  /**
   * @public
   * Update multi select look
   */
  update() {
    this.itemsData = this._getOptionsObject();
    this._buildOptionsList();
    this._buildItemsList();
  }

  /**
   * @public
   * Get multi selected options
   * @param {object} obj - must be object to assign data by reference (as return value)
   */
  getValues(obj) {
    obj.selected = [];

    Array.prototype.slice.call(this.element.querySelector('select[multiple]').selectedOptions).forEach(function(option){
      obj.selected.push({
        title: option.innerText,
        value: option.value
      })
    })
  }

}
