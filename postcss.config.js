const package = require('./package.json');
const banner = `${package.name} ${package.version} | built by ${package.author} | ${package.email}`;

module.exports = {
  plugins: {
    'autoprefixer': {
      browsers: ['> 1%', 'last 2 versions', 'not ie <= 9', 'iOS >= 8']
    },
    'postcss-banner': {
      banner: banner,
      important: true
    }
  },
}
